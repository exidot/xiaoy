#ifndef __SORT_H__
#define __SORT_H__
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * 打印
 */
extern void print(int *ptr, size_t count);

/**
 * 插入排序
 *
 * ptr: 待排序序列的指针
 * count: 待排序序列的数目
 */
extern void insertion_sort(int *ptr, size_t count);

/**
 * 归并排序
 */
extern void merge_sort(int *ptr, size_t start, size_t end);

/**
 * 冒泡排序
 */
extern void bubble_sort(int *ptr, size_t count);

/**
 * 快速排序
 */
extern void quick_sort(int *ptr, size_t start, size_t end);

#endif