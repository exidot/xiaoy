#include "sort.h"

/**
 * 打印
 */
void print(int *ptr, size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        printf("%d ", *(ptr + i));
    }
    printf("\n");
}

/**
 * 插入排序
 */
void insertion_sort(int *ptr, size_t count)
{
    for (size_t i = 1; i < count; i++)
    {
        int key = *(ptr + i);

        size_t j = i;
        while (j > 0 && *(ptr + j - 1) > key)
        {
            *(ptr + j) = *(ptr + j - 1);
            j = j - 1;
        }
        *(ptr + j) = key;
    }
}

/**
 * 归并排序辅助工具
 *
 * start: 起始索引
 * middle: 中间索引
 * end: 结束索引
 */
static void merge(int *ptr, size_t start, size_t middle, size_t end)
{
    size_t left_count = middle - start + 1;
    size_t right_count = end - middle;
    int *left = (int *)malloc(left_count * sizeof(int));
    int *right = (int *)malloc(right_count * sizeof(int));
    memcpy(left, ptr + start, left_count * sizeof(int));
    memcpy(right, ptr + start + left_count, right_count * sizeof(int));

    size_t left_i = 0;
    size_t right_i = 0;
    for (size_t i = start; i <= end; i++)
    {
        if (*(left + left_i) <= *(right + right_i))
        {
            *(ptr + i) = *(left + left_i);
            left_i++;
        }
        else
        {
            *(ptr + i) = *(right + right_i);
            right_i++;
        }
    }
    free(left);
    free(right);
}

/**
 * 归并排序
 *
 * start: 起始索引
 * end: 元素个数
 */
void merge_sort(int *ptr, size_t start, size_t end)
{
    if (start < end)
    {
        size_t middle = (end + start) / 2;
        merge_sort(ptr, start, middle);
        merge_sort(ptr, middle + 1, end);
        merge(ptr, start, middle, end);
    }
}

/**
 * 冒泡排序
 */
void bubble_sort(int *ptr, size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        // 将小元素移动到最前的方式
        for (size_t j = count - 1; j > i; j--)
        {
            if (*(ptr + j - 1) < *(ptr + j))
            {
                int temp = *(ptr + j - 1);
                *(ptr + j - 1) = *(ptr + j);
                *(ptr + j) = temp;
            }
        }
        // 将大元素移动到最后的方式
        // for (size_t j = 1; j < count - i; j++)
        // {
        //     if (*(ptr + j - 1) < *(ptr + j)){
        //         int temp = *(ptr + j - 1);
        //         *(ptr + j - 1) = *(ptr + j);
        //         *(ptr + j) = temp;
        //     }
        // }
    }
}

/**
 * 快速排序分区工具
 */
static size_t quick_partition(int *ptr, size_t start, size_t end)
{
    int item = *(ptr + end);  // 以最后一个元素为基准
    size_t i = start - 1;  // 记录小于 item 的元素位置. 注意当 start = 0 时, 无符号整数 i = start-1 将是很大的整数
    size_t j = start;
    while (j < end)
    {
        if (*(ptr + j) <= item)
        {
            i++;
            int temp = *(ptr + i);
            *(ptr + i) = *(ptr + j);
            *(ptr + j) = temp;
        }
        j++;
    }
    int temp = *(ptr + i + 1);
    *(ptr + i + 1) = item;
    *(ptr + end) = temp;
    return i + 1;
}

/**
 * 快速排序
 * 
 * start: 起始索引
 * end: 结束索引
 */
void quick_sort(int *ptr, size_t start, size_t end)
{
    if (start < end)
    {
        size_t middle = quick_partition(ptr, start, end);
        quick_sort(ptr, start, middle - 1);
        quick_sort(ptr, middle, end);
    }
}