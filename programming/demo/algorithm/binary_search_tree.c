#include "binary_search_tree.h"

/** 中序遍历 */
void bst_inorder_walk(node_t *tree)
{
    if (tree != NULL)
    {
        bst_inorder_walk(tree->left);
        printf("%d ", tree->value);
        bst_inorder_walk(tree->right);
    }
}

/** 搜索指定值所在节点 */
node_t *bst_search(node_t *tree, int value)
{
    // 树为空或该树的值为指定值, 则树就是要查找的节点
    if (tree == NULL || tree->value == value)
    {
        return tree;
    }
    // 数的值大于指定值, 则指定值处于树的左子树中, 否则处于树的右子树中
    if (tree->value > value)
    {
        // 从左子树中搜索指定值
        return bst_search(tree->left, value);
    }
    else
    {
        // 从树的右子树中搜索
        return bst_search(tree->right, value);
    }
}

/** 查找树的最小值 */
node_t *bst_minimum(node_t *tree)
{
    node_t *node = tree;
    // 最小节点就是二叉树的最左下位置的节点
    while (node->left != NULL)
    {
        node = node->left;
    }
    return node;
}

/** 查找树的最大值 */
node_t *bst_maximum(node_t *tree)
{
    node_t *node = tree;
    // 最大节点就是二叉树的最右下位置的节点
    while (node->right != NULL)
    {
        node = node->right;
    }
    return node;
}

/** 查找节点的后继节点
 * 后继节点是(中序)遍历(左父右)时, 该节点后的节点, 即大于该节点的最小节点
 */
node_t *bst_successor(node_t *node)
{
    // 节点的右子树的最小节点即为后继节点
    if (node->right != NULL)
    {
        return bst_minimum(node->right);
    }
    // 当右子树为空, 则查找该节点作为右子树元素的最近父代节点
    node_t *temp = node;
    node_t *parent = temp->parent;
    while (parent != NULL && temp == parent->right)
    {
        temp = parent;
        parent = temp->parent;
    }
    return parent;
}

/** 查找节点的前驱节点
 * 前驱节点是(中序)遍历时, 该节点前的节点, 即小于该节点的最大节点
 */
node_t *bst_predecessor(node_t *node)
{
    // 节点的左子树的最大节点即为前驱节点
    if (node->left != NULL)
    {
        return bst_maximum(node->left);
    }
    // 当左子树为空, 则查找该节点作为左子树元素的最近父代节点
    node_t *temp = node;
    node_t *parent = temp->parent;
    while (parent != NULL && temp == parent->left)
    {
        temp = parent;
        parent = temp->parent;
    }
    return parent;
}

/** 插入节点 */
void bst_insert(node_t *tree, node_t *node)
{
    // 查找插入的父节点
    node_t *parent = NULL;
    node_t *temp = tree;
    while (temp != NULL)
    {
        parent = temp;
        if (temp->value > node->value)
        {
            temp = temp->left;
        }
        else
        {
            temp = temp->right;
        }
    }
    if (parent == NULL)
    {
        tree = node;
    }
    else if (parent->value > node->value)
    {
        parent->left = node;
    }
    else
    {
        parent->right = node;
    }
}

/** 迁移节点工具
 * 
 * tree: 树, 指向树 root 节点
 * sub_tree: 子树
 * rep_tree: 替换 sub_tree 的子树
 */
static void bst_transplant(node_t *tree, node_t *sub_tree, node_t *rep_tree)
{
    // 子树的父节点为空, 表示 sub_tree 即为 root 节点
    if (sub_tree->parent == NULL)
    {
        tree = rep_tree;
    }
    else if (sub_tree = sub_tree->parent->left)  // 子树为左子树
    {
        sub_tree->parent->left = rep_tree;
    }
    else  // 子树为右子树
    {
        sub_tree->parent->right = rep_tree;
    }
}

/** 删除节点 */
void bst_delete(node_t *tree, node_t *node)
{
    if (node->left == NULL)
    {
        bst_transplant(tree, node, node->right);
    }
    else if (node->right = NULL)
    {
        bst_transplant(tree, node, node->left);
    }
    else
    {
        node_t *minimum = bst_minimum(node->right);
        if (minimum->parent != node)
        {
            bst_transplant(tree, minimum, minimum->right);
            minimum->right = node->right;
            minimum->right->parent = minimum;
        }
        bst_transplant(tree, node, minimum);
        minimum->left = node->left;
        minimum->left->parent = minimum;
    }
}