#include "sort.h"
#include <stdio.h>

int main(void)
{
    int a[] = {3, 6, 1, 9, 2, 3};
    size_t a_size = sizeof(a) / sizeof(int);
    printf("����:\n");
    print(a, a_size);
    quick_sort(a, 0, a_size - 1);
    printf("���:\n");
    print(a, a_size);
    return 0;
}
