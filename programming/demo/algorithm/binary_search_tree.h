#ifndef __BINARY_SEARCH_TREE_H__
#define __BINARY_SEARCH_TREE_H__
#include <stdio.h>
#include <stdlib.h>

typedef struct node_t node_t;

struct node_t
{
    int value;
    node_t *parent;
    node_t *left;
    node_t *right;
};

/** 中序遍历 */
extern void bst_inorder_walk(node_t *tree);

/** 搜索指定值的节点 */
extern node_t *bst_search(node_t *tree, int value);

/** 查找最小值的节点 */
extern node_t *bst_minimum(node_t *tree);

/** 查找最大值的节点 */
extern node_t *bst_maximum(node_t *tree);

/** 查找节点的后继节点 */
extern node_t *bst_successor(node_t *node);

/** 查找节点的前驱节点 */
extern node_t *bst_predecessor(node_t *node);

/** 插入节点 */
extern void bst_insert(node_t *tree, node_t *node);

/** 删除节点 */
extern void bst_delete(node_t *tree, node_t *node);

#endif
