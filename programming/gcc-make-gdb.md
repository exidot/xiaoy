## GCC, Make和GDB 笔记


### GCC 常用参数

-E: 仅预处理, 类似 cpp 命令

-S: 生成汇编代码, 类似 as 命令

-c: 编译

-o: 指定生成文件名

-I: 指定头文件搜索文件夹, 可多个. 在环境变量 C_INCLUDE_PATH 前搜索. -I. -I/usr/lib/python3/include

-L: 指定库文件搜索文件夹, 可多个. 在环境变量 LIBRARY_PATH 前搜索. -L. -L/usr/lib/python3/lib

-l: 指定依赖库, 可多个. 注意若同时存在静态库和动态库(共享库), 优先使用动态库, 环境变量 LD_LIBRARY_PATH 用于指定非标准位置的动态库路径. -lm -lgtk

-static: 当同时存在静态库和动态库时, 强制使用静态库链接.

-fPIC: 生成位置无关的代码, 用于动态链接

-shared: 构建动态库, 需要与 -fPIC 一同使用

-D: 定义预处理宏. -DTEST

-save_temps: 保存编译过程中生成的中间文件

-g: 生成调试信息, 用于 GDB 调试

-O0, -O1, -O2, -O3: 优化等级, -O 同 -O0 是不开启优化, 默认为 -O2

-std: 指定 C 标准. -std=c90 (同-ansi)

-pedantic: 禁用所有 GNU C 扩展

-Wall: 开启所有警告

-w: 关闭警告


### Makefile 文件

* 基本规则

```
# 这里是注释
targets : prerequisites
    commands
```
> targets: 目标  
> prerequisites : 先决条件  
> commands: 命令  
> 当 prerequisites 有变化时, 运行 make [target] 就会执行 commands; 当 prerequisites 没有变化时, 运行 make [target] 不会执行 commands;

```
# 例如
main : main.c func.c
    gcc main.c func.c -o main
```

* 包含 Makefile

```
include Makefile.A Makefile.B
```


* 变量

```
# 延迟赋值
var1 = main.o hello.o
# 立即赋值
var2 := main.o hello.o
# 当 var3 没有被赋值时赋值
var3 ?= main.o hello.o
# 追加
var4 += main.o hello.o
# 覆盖
override var5 = var5  # 覆盖命令行传递的变量
# 使用变量
obj : $(var1)
    @echo 输出
# 变量替换规则
var5 = $(var1:.o=.c)  # 将 var1 中 .o 替换为 .c
```


* 条件执行

ifeq .. else .. endif  # 判断相等执行

ifneq .. else .. endif  # 判断不相等执行

ifdef .. else .. endif  # 判断已定义时执行

ifndef .. else .. endif  # 判断未定义时执行


* 函数

$(subst src, dst, vars): 将 vars 中的 src 替换为 dst

$(patsubst re, dst, vars): 将 vars 匹配 re 的替换为 dst. re 和 dst 中可以含有 %, 代表单词

$(strip vars): 去除 vars 中多余的空字符

$(findstring dst, vars): 搜索 vars 中的 dst, 若存在返回 dst, 不存在返回空

$(filter re.., vars): 筛选 vars 中符合模式 re (可多个)的单词

$(filter-out re.., vars): 与函数 filter 功能相反

$(sort vars): 排序

$(word n, vars): 取 vars (从1计数)中第 n 个单词

$(wordlist n1, n2, vars): 取 vars 中第 n1 到 n2 个单词

$(words vars): 获取 vars 中有多少个单词

$(firstword vars): 获取 vars 的第一个单词

$(dir vars): 获取 vars 中每个单词的目录名

$(notdir vars): 获取 vars 中每个单词的文件名

$(suffix vars): 获取 vars 中每个单词的后缀名

$(basename vars): 获取 vars 中每个单词去除后缀的部分

$(addsuffix dst, vars): 为 vars 中每个单词添加后缀

$(addprefix dst, vars): 为 vars 中每个单词添加前缀

$(join vars1, vars2): 将 vars1 和 vars2 对应位置连接

$(wildcard re): 获取当前目录下符合模式 re 的文件名

$(foreach var, vars, command): 迭代 vars 中的每个单词赋值给 var, 执行 command 返回

$(if condi, var1, var2): 如果 condi 非空, 返回 var1, 否则返回 var2

$(call vars, param1, params2...): 将 param 填充到 vars 中的 $(1), $(2)...

$(value var): 不进一步展开 var

$(eval vars): [用到时在说吧]

$(origin var): 获取变量 var 的出处. undefined: 未定义, default: 默认定义, environment: 环境变量, environment override: 覆盖的环境变量, file: 在 Makefile 中定义, command line: 命令行中定义, override: 覆盖定义, automatic: 自动化变量

$(shell command): 执行 shell 命令



### Make 常用参数

-C --directory: 在读取 Makefile 之前进入指定目录

-f --file --makefile: 指定 Makefile 文件名

-I --include-dir: 指定搜索 Makefile 文件的目录

-j --jobs: 并行数

-s --silent --quiet: 不显示命令执行过程


### GDB 常用命令

r run: 运行

b break: 下断点. 后跟行号或函数名, 后再跟条件

clear 行号: 清楚指定行号的断点

info b: 显示所有断点

delete 断点序号: 删除指定断点

disable 断点序号: 禁用指定断点

enable 断点序号: 启用指定断点

delete breakpoints: 删除所有断点

c continue: 继续执行

n next: 单步跳过

s step: 单步步入

until: 执行到循环结束

until+行号: 执行到指定行

call: 执行函数

return: 在函数直接返回, 后跟返回值

l list: 显示当前的源码. 后可跟行号或函数, 显示指定行号或函数的源码, 回车可以继续显示

p print 表达式: 显示表达式的值

display 表达式: 实时监控表达式的值

watch 表达式: 当表达式的值变化时输出

whatis: 查看类型

info locals: 查看变量

info variables: 显示变量符号

info reg: 查看寄存器值

info functions: 查看函数

info args: 查看函数参数

info frame: 显示当前帧

info stack: 显示栈

acktrace: 查看调用堆栈

set var: 设置 var 的值

set args: 设置执行参数

show args: 显示执行参数

info program: 显示程序信息

file: 载入程序

attach: 附加程序

layout: src 显示源码, asm 显示反汇编, regs: 显示寄存器, split: 分割

q quit: 退出

### bin 工具

ar: 用于创建静态链接库.

file: 查看文件类型

nm: 查看符号表

ldd: 查看引用的共享库

ld: 链接程序
