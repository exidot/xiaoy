## 我的技能树

* 技能状态

  待学: 还未学习

  了解: 已了解该技术, 能够阅读, 但无法写

  掌握: 能够读写技术 (掌握以上即可打勾)

  熟练: 能够顺畅读写技术

  精通: *登峰造极*



### [编程技术](./programming/)
* [ ] C 语言: 了解
* [ ] C++ 语言: 待学
* [ ] Rust 语言: 待学
* [x] C# 语言: 掌握
* [x] Python 语言: 熟练
* [x] TypeScript (JavaScript): 掌握
* [ ] 算法: 了解
* [ ] 设计模式: 了解
* [ ] UML (EA): 了解
* [x] PowerShell: 掌握
* [x] Asp.Net Core: 掌握
* [x] DJango: 掌握
* [ ] FastApi: 了解
* [ ] Sqlalchemy: 了解
* [ ] Nginx: 了解
* [x] PostgreSQL: 掌握
* [x] MySQL: 掌握
* [x] Redis: 掌握
* [x] Vue: 掌握
* [ ] Vite: 了解
* [x] React: 掌握
* [x] Meterial UI: 掌握
* [ ] TCP 基础: 了解
* [ ] 路由技术: 待学



### 数学
* [ ] 概率: 了解
* [ ] 微积分: 了解
* [ ] Matlab: 待学
* [ ] Mathematica: 待学



### 电磁学
* [ ] Simulink: 待学
* [ ] SystemModeler: 待学
* [ ] KiCAD: 待学
* [ ] Spice: 待学
* [ ] FreeCAD: 待学



### 逆向工程

* [ ] IDA Pro: 待学
* [ ] Frida: 待学
* [ ] x64dbg: 待学



### 音乐

* [ ] 乐理: 待学
* [ ] MuseScore: 待学



### 图像

* [ ] Blender: 待学
* [ ] Godot: 待学
* [ ] Audacity: 待学
* [ ] FFmpeg: 了解
* [ ] inkscape: 了解
* [ ] Krita: 了解
* [ ] Gimp: 待学
